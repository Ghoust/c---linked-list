#include <assert.h>
#include "bookArray.h"
#include <stdlib.h>
#include <string.h>

IBookArray bookaNew() {
	IBookArray this = calloc(1, sizeof(struct BookArray));
	this->book = NULL;
	this->next = NULL;
	return this;
}

void bookaDelete(IBookArray this) {
}

void bookaAppend(IBookArray this, IBook book) {
}

IBook bookaGet(IBookArray this, int i) {
	return NULL;
}

int bookaIndexOf(IBookArray this, IBook book) {
	return 0;
}

void bookaInsertAt(IBookArray this, int i, IBook book) {
}

void bookaRemoveAt(IBookArray this, int i) {
}

void bookaRemoveLast(IBookArray this) {
}

void bookaSet(IBookArray this, int i, IBook book) {
}

int bookaSize(IBookArray this) {
	return 0;
}
